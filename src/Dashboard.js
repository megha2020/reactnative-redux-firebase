import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Text, View } from "react-native";
/**
 * @author
 * @class Dashboard
 **/

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Text style={{ fontSize: 28, textAlign: "center", fontWeight: "bold" }}>
          WellCome Dashboard
        </Text>
        <Text style={{ fontSize: 20, textAlign: "center" }}>
          {this.props.user.email}
        </Text>
      </View>
    );
  }
}

Dashboard.propTypes = {
  user: PropTypes.object,
  loading: PropTypes.bool,
};
const mapStateToProps = (state) => ({
  user: state.userReducer.user,
  loading: state.userReducer.loading,
});
export default connect(
  mapStateToProps,
  null
)(Dashboard);
