import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import { login } from "../actions/userAction";
// import LoginForm from "./LoginForm";
// import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ToastAndroid,
} from "react-native";
import { login } from "./actions/userAction";
import { db as firebaseApp } from "./config";

/**
 * @author
 * @class Login
 **/

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  handleLogin = (email, password) => {
    this.props.login(email, firebaseApp);
    this.setState({ password: password });
  };
  componentDidUpdate() {
    const { user, loading } = this.props;
    if (user && !loading) {
      if (user.password === this.state.password) {
        // this.props.handlePage(2);
        ToastAndroid.show("Login Successfull", ToastAndroid.SHORT);
        this.props.navigation.navigate("Dashboard");
      } else {
        // ToastAndroid.show("Wrong username/password", ToastAndroid.SHORT);
      }
    }
  }

  validation = () => {
    var errorField = null;
    const { email, password } = this.state;
    if (email === "") {
      errorField = " Email Required";
    } else if (password === "") {
      errorField = "Password Required";
    }
    if (errorField) {
      alert(errorField);
    } else {
      // dispatch(fetchdata(email, password));
      this.handleLogin(email, password);
    }
  };

  render() {
    // return <LoginForm {...this.props} handleLogin={this.handleLogin} />;
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        // backgroundColor: "#212121",
        alignItems: "center",
        justifyContent: "center",
      },
      title: {
        fontSize: 40,
        marginBottom: 30,
        marginTop: 6,
        color: "#000",
      },
      foodInput: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderWidth: 0.5,
        borderRadius: 6,
        width: Dimensions.get("screen").width * 0.8,
        paddingHorizontal: 5,
      },
      image: {
        width: 120,
        height: 120,
        borderColor: "orange",
        borderWidth: 2,
        borderRadius: 100,
      },
      button: {
        backgroundColor: "blue",
        justifyContent: "center",
        alignItems: "center",
      },
    });
    const { email, password } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Sign in</Text>
        <TextInput
          value={email}
          placeholder="Email"
          style={styles.foodInput}
          onChangeText={(email) => this.setState({ email: email })}
          keyboardType="email-address"
        />

        <TextInput
          value={password}
          placeholder="Password"
          style={{ ...styles.foodInput, marginTop: "5%" }}
          onChangeText={(password) => this.setState({ password: password })}
        />

        <TouchableOpacity
          style={{
            ...styles.button,
            ...styles.foodInput,
            marginVertical: 30,
            height: 50,
          }}
          onPress={() => {
            this.validation();
            // navigation.navigate("Dashboard");
          }}
        >
          <Text
            style={{
              fontSize: 22,
              color: "white",
              textAlign: "center",
            }}
          >
            Submit
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={() => this.props.navigation.navigate("Registration")}
        >
          <Text style={{ fontSize: 20 }}>Don't have an account? Sign Up</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  user: PropTypes.object,
  loading: PropTypes.bool,
};
const mapStateToProps = (state) => ({
  user: state.userReducer.user,
  loading: state.userReducer.loading,
});
export default connect(
  mapStateToProps,
  { login }
)(Login);
