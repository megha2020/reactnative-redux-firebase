import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  ToastAndroid,
} from "react-native";
import { register } from "./actions/userAction";
import { db as firebaseApp } from "./config";

/**
 * @author
 * @class Registration
 **/

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNamee: null,
      lastName: null,
      age: 0,
      email: null,
      phoneNo: 0,
      passwor: null,
      step: 1,
    };
  }

  header = () => {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderWidth: 0.5,
        borderRadius: 6,
        width: Dimensions.get("screen").width * 0.8,
        paddingHorizontal: 5,
      },
      button: {
        backgroundColor: "blue",
        justifyContent: "center",
        alignItems: "center",
      },
    });
    return (
      <View style={{ flexDirection: "row", marginBottom: 40 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "gray",
            height: 40,
            justifyContent: "center",
          }}
        >
          <TouchableOpacity onPress={() => this.setState({ step: 1 })}>
            <Text
              style={{
                textAlign: "center",
                color: this.state.step === 1 ? "white" : "blue",
                fontSize: 22,
              }}
            >
              Step 1
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: "gray",
            height: 40,
            justifyContent: "center",
          }}
        >
          <TouchableOpacity onPress={() => this.setState({ step: 2 })}>
            <Text
              style={{
                textAlign: "center",
                color: this.state.step === 2 ? "white" : "blue",
                fontSize: 22,
              }}
            >
              Step 2
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  formOne = () => {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderWidth: 0.5,
        borderRadius: 6,
        width: Dimensions.get("screen").width * 0.8,
        paddingHorizontal: 5,
      },
      button: {
        backgroundColor: "blue",
        justifyContent: "center",
        alignItems: "center",
      },
    });
    return (
      <>
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ firstName: text })}
          value={this.state.firstName}
          placeholder="Enter First Name"
        />
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ lastName: text })}
          value={this.state.lastName}
          placeholder="Enter Last Name"
        />
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ age: text })}
          value={this.state.age}
          maxLength={2}
          keyboardType="numeric"
          placeholder="Enter Your Age"
        />
      </>
    );
  };
  formTwo = () => {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderWidth: 0.5,
        borderRadius: 6,
        width: Dimensions.get("screen").width * 0.8,
        paddingHorizontal: 5,
      },
      button: {
        backgroundColor: "blue",
        justifyContent: "center",
        alignItems: "center",
      },
    });
    return (
      <>
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ email: text })}
          value={this.state.email}
          placeholder="Enter Email"
          keyboardType="email-address"
        />
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ phoneNo: text })}
          value={this.state.phoneNo}
          keyboardType="number-pad"
          maxLength={10}
          placeholder="Enter Phone No."
        />
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ password: text })}
          value={this.state.password}
          placeholder="Enter Password"
        />
      </>
    );
  };

  save = () => {
    const { firstName, lastName, age, email, phoneNo, password } = this.state;
    const data = {
      firstName: firstName,
      lastName: lastName,
      age: age,
      email: email,
      phoneNo: phoneNo,
      password: password,
    };
    this.props.register(data, firebaseApp);
    this.props.navigation.navigate("Login");
    // console.log("Save to Database: ", data);
  };
  validation = () => {
    const nameRegex = /^([a-zA-Z0-9]+|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{1,}|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{3,}\s{1}[a-zA-Z0-9]{1,})$/;
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var errorField = null;
    const {
      firstName,
      lastName,
      age,
      email,
      phoneNo,
      password,
      step,
    } = this.state;
    if (step === 1) {
      if (!nameRegex.test(firstName) || firstName === null) {
        errorField = "invalid First Name";
      } else if (!nameRegex.test(lastName) || lastName === null) {
        errorField = "invalid Last Name";
      } else if (age < 18 || age > 50) {
        errorField = "invalid Age Range (18 to 50)";
      }
      if (errorField) {
        alert(errorField);
      } else {
        this.setState({ step: 2 });
      }
    } else {
      if (!emailRegex.test(email) || email === null) {
        errorField = "invalid Email";
      } else if (phoneNo > 9999999999 || phoneNo < 1000000000) {
        errorField = "invalid Phone No.";
      } else if (password.length < 6) {
        errorField = "invalid Password. Minimum length : 6";
        ``;
      }
      if (errorField) {
        ToastAndroid.show(errorField, ToastAndroid.SHORT);
      } else {
        // alert("Saved to Database");
        ToastAndroid.show("Sign up successfull", ToastAndroid.SHORT);
        this.save();
      }
    }
  };

  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        borderWidth: 0.5,
        borderRadius: 6,
        width: Dimensions.get("screen").width * 0.8,
        paddingHorizontal: 5,
      },
      button: {
        backgroundColor: "blue",
        justifyContent: "center",
        alignItems: "center",
      },
    });
    return (
      <View style={styles.container}>
        <SafeAreaView>
          {this.header()}
          {this.state.step === 1 ? this.formOne() : this.formTwo()}
          <TouchableOpacity onPress={this.validation}>
            <View
              style={{
                ...styles.button,
                ...styles.input,
                marginVertical: 30,
                height: 50,
              }}
            >
              <Text style={{ color: "white", fontSize: 22 }}>
                {this.state.step === 1 ? "Save & Continue" : "Sign up"}
              </Text>
            </View>
          </TouchableOpacity>
        </SafeAreaView>
        <TouchableOpacity
          style={{ marginTop: 10 }}
          onPress={() => this.props.navigation.navigate("Login")}
        >
          <Text style={{ fontSize: 20 }}>
            Allready have an account? Sign In
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

Registration.propTypes = {
  register: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};
const mapStateToProps = (state) => ({
  loading: state.userReducer.loading,
});
export default connect(
  mapStateToProps,
  { register }
)(Registration);
