import { SAVE_USER, FETCH_USER } from "../actions/types";

initialState = {};

const foodReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_USER:
      return {
        ...state,
        user: action.payload,
      };
    case FETCH_USER:
      return {
        ...state,
        user: action.payload,
      };

    default:
      return state;
  }
};

export default foodReducer;
