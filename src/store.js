import { createStore, combineReducers, applyMiddleware } from "redux";
import userReducer from "./reducers/userReducer";
import thunk from "redux-thunk";

const initialState = {};

const rootReducer = combineReducers({
  userReducer: userReducer,
});

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

export default store;
