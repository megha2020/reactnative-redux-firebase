import { SAVE_USER, FETCH_USER } from "./types";
import { db } from "../config";
import { useDispatch } from "react-redux";

export const savedata = (data) => {
  db.ref("/users").push(data);
  return {
    type: SAVE_USER,
    payload: data,
  };
};

export const fetchdata = async (email, password) => {
  //

  db.ref("/users")
    .orderByChild("email")
    .equalTo(email)
    .on("value", (snapshot) => {
      // return {
      //   type: FETCH_USER,
      //   payload: { ...snapshot.val() },
      // };

      if (snapshot.val() != null && snapshot.val().password === password) {
        return {
          type: FETCH_USER,
          payload: { ...snapshot.val(), valid: true },
        };
      } else {
        return {
          type: FETCH_USER,
          payload: { valid: false },
        };
      }
    });
};
