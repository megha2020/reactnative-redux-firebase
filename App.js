import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "./src/Login";
import Dashboard from "./src/Dashboard";
import Registration from "./src/Registration";
import { NavigationContainer } from "@react-navigation/native";

const Stack = createStackNavigator();

export default (AppStack = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          // title: 'Cheetah Coding',
          headerTintColor: "orange",
          headerShown: false,
          // headerStyle: {
          //   backgroundColor: "black",
          // },
        }}
      />
      <Stack.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          headerTintColor: "orange",
          headerShown: false,
          // headerStyle: {
          //   backgroundColor: "black",
          // },
        }}
      />
      <Stack.Screen
        name="Registration"
        component={Registration}
        options={{
          headerTintColor: "orange",
          headerShown: false,
          // headerStyle: {
          //   backgroundColor: "black",
          // },
        }}
      />
    </Stack.Navigator>
  </NavigationContainer>
));
